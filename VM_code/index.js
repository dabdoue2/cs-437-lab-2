document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "192.168.137.166";   // the IP address of your Raspberry PI
var bluetooth_addr = "E4:5F:01:00:4C:D6";
var bluetooth_port = 4;
var nodeConsole = require('console');
var myConsole = new nodeConsole.Console(process.stdout, process.stderr);
var blue_message = ""
var bluetooth_message_count = 0

document.addEventListener("DOMContentLoaded", function() {
    update_data();
    get_bluetooth_message();

});

var return_data = function()
{
    // var text = "hi";
    const fs = require('fs')

    fs.readFile('/home/dabdoue/iot-labs/iot-lab-2/electron/message.txt', 'utf8', (err, data) => {
        blue_message = data.toString();

        if (err) {
            // return data;
            console.error(err)
        }
    })
    // fs.writeFile('/home/dabdoue/iot-labs/iot-lab-2/electron/input1.txt', data, err => {
    //     if (err) {
    //         console.error(err)
    //     }
    //     //file written successfully
    // })
    // blue_message = text;
};

var get_bluetooth_message = function()
{

    
    // let input = document.getElementById("message").value;
    bluetooth_message_count += 1;
    const fs = require('fs')
    // const spawn = require("child_process").spawn;

    fs.appendFile('/home/dabdoue/iot-labs/iot-lab-2/electron/input.txt', bluetooth_message_count.toString(), err => {
        if (err) {
            console.error(err)
        }
        //file written successfully
    })
     

};

function client()
{
    // var input = document.getElementById("message").value;
    // document.getElementById("message").innerHTML = blue_message;
    
    const net = require('net');

    // var dataToSend;
    // // spawn new child process to call the python script
    // const python = spawn('python3', ['bt_client.py', input]);
    // // collect data from script
    // python.stdout.on('data', function (data) {
    // console.log('Pipe data from python script ...');
    // dataToSend = data.toString();
    // });
    // document.getElementById("message").innerHTML = dataToSend;

    // // in close event we are sure that stream from child process is closed
    // python.on('close', (code) => {
    // console.log(`child process close all stdio with code ${code}`);
    // const bluetooth = require('node-bluetooth');


    // bluetooth.connect(bluetooth_addr, bluetooth_port, function(err, connection){
    //     if(err) return console.error(err);
       
    //     connection.on('data', (buffer) => {
    //         document.getElementById("message").innerHTML = buffer.toString();

    //         console.log('received message:', buffer.toString());
    //     });
       
    //     connection.write(new Buffer(input, 'utf-8'), () => {
    //       console.log("wrote");
    //     });
    //   });





    const client = net.createConnection({ port: server_port, host: server_addr }, () =>
    {
        // // 'connect' listener.
        // console.log('connected to server!');
        // // send the message
        client.write('hello');
    });

    // get the data from the server
    client.on('data', (data) =>
    {
        str_data = data.toString();
        all_data = str_data.split(",");
        document.getElementById("battery").innerHTML = all_data[0];
        document.getElementById("temperature").innerHTML = all_data[1];
        return_data();
        document.getElementById("blue_mes").innerHTML = blue_message;


        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () =>
    {
        console.log('disconnected from server');
    });


}

// for detecting which key is been pressed w,a,s,d
function updateKey(e)
{

    e = e || window.event;
    const net = require('net');
    dir = ""
    if (e.keyCode == '87')
    {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        dir = "up"
    }
    else if (e.keyCode == '83')
    {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        dir = "down"
    }
    else if (e.keyCode == '65')
    {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        dir = "left"
    }
    else if (e.keyCode == '68')
    {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        dir = "right"
    }

    const client = net.createConnection({ port: server_port, host: server_addr }, () =>
    {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        if (dir != "")
        {
            client.write(`${dir}`);
        }
    });

}

// reset the key to the start state 
function resetKey(e)
{

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 50ms
function update_data()
{
    setInterval(function ()
    {
        // get image from python server
        client();
        updateKey(e);
    }, 50);
}
