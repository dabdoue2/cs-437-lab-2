import picar_4wd as fc
import time
import scipy

speed = 20

turned_right = False

turned_left = False

time_left_of_course = 0
time_turned_left = 0
time_right_of_course = 0
time_turned_right = 0

time_before_first_turn = 0


def reset_vars():
    turned_right = False
    turned_left = False

    time_left_of_course = 0
    time_turned_left = 0
    time_right_of_course = 0
    time_turned_right = 0

    time_before_first_turn = 0


def main():
    global turned_right

    global turned_left

    global time_left_of_course
    global time_turned_left
    global time_right_of_course
    global time_turned_right

    global time_before_first_turn

    total_time_moving = time.time() + 10
    while time.time() < total_time_moving:
        distances_list = fc.get_distances()
        if not distances_list or len(distances_list) < 9:
            continue

        distances_in_front = distances_list[3:10]
        print(distances_in_front)

        min_dist = 30
        min_dist_arr = [min_dist] * 5

        left_distances = scipy.mean(distances_in_front[0:2])
        right_distances = scipy.mean(distances_in_front[5:7])

        if distances_in_front[1:6] > min_dist_arr:
            if turned_left == True or turned_right == True:
                if right_distances < min_dist:
                    fc.forward(speed)
                    time_left_of_course = time.time()
                elif left_distances < min_dist:
                    fc.forward(speed)
                    time_right_of_course = time.time()
                else:
                    if time_right_of_course != 0:

                        if time_turned_left != 0:
                            net_time_turned_left = time_turned_left - time_before_first_turn
                        if time_turned_right != 0:
                            net_time_turned_right = time_turned_right - time_before_first_turn

                        if net_time_turned_right > net_time_turned_left:
                            # turned more right than left,
                            # therefore need to turn left, then drive forward, then turn right to be back on course
                            time_to_turn_left = time.time() + net_time_turned_right
                            while time.time() < time_to_turn_left:
                                fc.turn_left(speed)

                            time_to_redirect = time_right_of_course - time_turned_right + time.time()
                            while time.time() < time_to_redirect:
                                fc.forward(speed)

                            time_to_turn_right = time.time() + net_time_turned_right
                            while time.time() < time_to_turn_right:
                                fc.turn_right(speed)
                        else:
                            # turned more left than right,
                            # therefore need to turn right, then drive forward, then turn left to be back on course
                            time_to_turn_right = time.time() + net_time_turned_left
                            while time.time() < time_to_turn_right:
                                fc.turn_right(speed)

                            time_to_redirect = time_left_of_course - time_turned_left + time.time()
                            while time.time() < time_to_redirect:
                                fc.forward(speed)

                            time_to_turn_left = time.time() + net_time_turned_left
                            while time.time() < time_to_turn_left:
                                fc.turn_left(speed)

                        reset_vars()

            else:
                fc.forward(speed)

        else:
            print(right_distances)
            print(left_distances)
            if time_before_first_turn == 0:
                time_before_first_turn = time.time()
            if right_distances > left_distances:

                fc.turn_right(speed)
                turned_right = True
                time_turned_right = time.time()
            else:
                fc.turn_left(speed)
                turned_left = True
                time_turned_left = time.time()
            # fc.stop()
            # quit()

        # ten_second_map.append(distances_in_front)
        # if (stats.mode(scan_list[4:9])[0][0] >= 1):
        #     print("path clear")
        # else:
        #     print("path blocked")

        # if time.time() > timeout:
        #     break
    print("time done")
    # print(ten_second_map)


if __name__ == "__main__":
    try:
        main()
    finally:
        fc.stop()
