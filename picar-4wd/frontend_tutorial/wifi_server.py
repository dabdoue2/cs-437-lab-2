import socket
from picar_4wd.utils import power_read, cpu_temperature
import picar_4wd as fc
import time

HOST = "192.168.137.166"  # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()

    up_encoded = "up".encode()
    down_encoded = "down".encode()
    left_encoded = "left".encode()
    right_encoded = "right".encode()
    try:
        while 1:
            client, clientInfo = s.accept()
            # print("server recv from: ", clientInfo)
            # receive 1024 Bytes of message in binary forma
            data = client.recv(1024)

            # decoded = data.decode()
            if (data == up_encoded):
                time_until_stop = time.time() + 0.1
                while time.time() < time_until_stop:
                    fc.forward(10)
                fc.stop()

            elif (data == down_encoded):
                time_until_stop = time.time() + 0.1
                while time.time() < time_until_stop:
                    fc.backward(10)
                fc.stop()

            elif (data == left_encoded):
                time_until_stop = time.time() + 0.1
                while time.time() < time_until_stop:
                    fc.turn_left(10)
                fc.stop()

            elif (data == right_encoded):
                time_until_stop = time.time() + 0.1
                while time.time() < time_until_stop:
                    fc.turn_right(10)
                fc.stop()

            if data != b"":
                total_data = str(power_read()) + "," + str(cpu_temperature())
                print(total_data)
                client.sendall(total_data.encode())  # Echo back to client
            # print(power_read())

    except:
        print("Closing socket")
        client.close()
        s.close()
