import bluetooth

# The address of Raspberry PI Bluetooth adapter on the server. The server might have multiple Bluetooth adapters.
hostMACAddress = "E4:5F:01:00:4C:D6"
port = 4
backlog = 1
size = 1024
s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
s.bind((hostMACAddress, port))
s.listen(backlog)
print("listening on port ", port)
prev_message = ""
try:
    client, clientInfo = s.accept()
    while 1:
        print("server recv from: ", clientInfo)
        data = client.recv(size)

        if data:
            decoded = data.decode()
            if decoded != prev_message:
                prev_message = decoded
                message = "Bluetooth data sent " + decoded + " times."
                print(message)
                client.send(message.encode())  # Echo back to client
except:
    print("Closing socket")
    client.close()
    s.close()
