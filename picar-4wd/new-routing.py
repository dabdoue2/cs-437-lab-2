# import sys
# import os.path
# detect_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
#               + '/tflow_examples/examples/lite/examples/object_detection/raspberry_pi/')
# sys.path.append(detect_dir)

import detect
import picar_4wd as fc
import time
import argparse
import subprocess
import numpy
# import sys
# import scipy


speed = 1
stop_sign_dist = 50
stop_sign_dist_arr = [stop_sign_dist] * 7
min_dist = 50
min_dist_arr = [min_dist] * 5
objs = ""
process = 0
num_stop_signs = 0
has_seen_stop_sign = 0
time_till_stop = 0


def turn_right_90(time_to_turn):
    time_end_turn = time.time() + time_to_turn

    while time.time() < time_end_turn:
        fc.turn_right(speed)


def turn_left_90(time_to_turn):
    time_end_turn = time.time() + time_to_turn

    while time.time() < time_end_turn:
        fc.turn_left(speed)


def drive_forward(time_to_drive_forward):
    global speed
    time_end_driving_forward = time.time() + time_to_drive_forward

    while time.time() < time_end_driving_forward:
        fc.forward(speed)


def detected_objs():
    global objs
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--model',
        help='Path of the object detection model.',
        required=False,
        default='efficientdet_lite0.tflite')
    parser.add_argument(
        '--cameraId', help='Id of camera.', required=False, type=int, default=0)
    parser.add_argument(
        '--frameWidth',
        help='Width of frame to capture from camera.',
        required=False,
        type=int,
        default=640)
    parser.add_argument(
        '--frameHeight',
        help='Height of frame to capture from camera.',
        required=False,
        type=int,
        default=300)
    parser.add_argument(
        '--numThreads',
        help='Number of CPU threads to run the model.',
        required=False,
        type=int,
        default=2)
    parser.add_argument(
        '--enableEdgeTPU',
        help='Whether to run the model on EdgeTPU.',
        action='store_true',
        required=False,
        default=False)
    args = parser.parse_args()

    objs = detect.run(args.model, int(args.cameraId), args.frameWidth, args.frameHeight,
                      int(args.numThreads), bool(args.enableEdgeTPU))


def main():
    global speed
    global process
    global num_stop_signs
    global has_seen_stop_sign
    global time_till_stop
    global objs
    process = subprocess.Popen(
        ['python3', 'detect.py'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    time.sleep(10)
    total_time_moving = time.time() + 20
    while time.time() < total_time_moving:

        global min_dist_arr
        distances_list = fc.get_distances()

        # max(set(distances_list), key=distances_list.count) < 0
        # print(distances_list)
        if not distances_list or len(distances_list) < 9:
            continue

        distances_in_front = distances_list[3:10]

        for i in range(len(distances_in_front)):
            if distances_in_front[i] < 0:
                distances_in_front[i] = 100

        print(distances_in_front)
        print("mean distance", numpy.mean(distances_in_front[1:6]))

        f = open("objects.txt", "r")
        objs = f.read()
        print(objs)

        if has_seen_stop_sign == 0 and objs is not "" and "stop sign" in objs:
            has_seen_stop_sign = 1
            time_till_stop = time.time() + 2

        elif num_stop_signs == 0 and time.time() > time_till_stop:
            num_stop_signs = 1
            time_to_stop = time.time() + 2
            while time.time() < time_to_stop:
                fc.stop()

        elif numpy.mean(distances_in_front[1:6]) < 55:

            # elif num_stop_signs == 0 and objs is not "" and "stop sign" in objs:
            #     time_to_stop = time.time() + 2
            #     num_stop_signs = 1
            #     while time.time() < time_to_stop:
            #         fc.stop()

            if objs is not "" and ("clock" in objs or "tv" in objs or "cell phone" in objs or "tablet" in objs or "laptop" in objs):
                print("dont hit technology")
                quit()

            else:
                turn_left_90(1.3)
                drive_forward(0.8)
                turn_right_90(1.2)
                drive_forward(2.2)
                turn_right_90(1.1)
                drive_forward(0.8)
                turn_left_90(1.2)
                fc.forward(speed)
        else:
            fc.forward(speed)

    print("time done")
    process.kill()
    # print(ten_second_map)


if __name__ == "__main__":
    try:
        main()
    finally:
        process.kill()
        fc.stop()
