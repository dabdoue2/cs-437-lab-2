import picar_4wd as fc
import time
from scipy import stats


def main():
    ten_second_map = []
    timeout = time.time() + 20   # 10 seconds from now
    while True:
        distances_list = fc.get_distances()
        if not distances_list or len(distances_list) < 9:
            continue

        print(distances_list[3:10])
        ten_second_map.append(distances_list[3:10])
        # if (stats.mode(scan_list[4:9])[0][0] >= 1):
        #     print("path clear")
        # else:
        #     print("path blocked")

        if time.time() > timeout:
            break

    print(ten_second_map)


if __name__ == "__main__":
    try:
        main()
    finally:
        fc.stop()
